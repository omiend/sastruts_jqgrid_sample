<%@page pageEncoding="UTF-8"%>
<html>
<head>
<title>ログイン - Yocal</title>
<link type="text/css" href="${f:url('/css/style.css')}" rel="stylesheet" />
</head>
<body>
<h1>Welcome to the Next Generation</h1>
<html:messages id="m" message="true">
<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>INFO:</strong>${f:h(m)}</p>
</div></div></html:messages>
<html:errors />
<a href="customer">jqGrid Sample</a>
</body>
</html>
