<%@page pageEncoding="UTF-8"%>
<html>
<head>
<title>顧客情報</title>
<link type="text/css" href="${f:url('/css/style.css')}" rel="stylesheet" />
<link type="text/css" href="${f:url('/css/redmond/jquery-ui-1.8.19.custom.css')}" rel="stylesheet" />
<link type="text/css" href="${f:url('/css/ui.jqgrid.css')}" rel="stylesheet" />
<script type="text/javascript" src="${f:url('/js/jquery-1.7.2.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/jquery-ui-1.8.19.custom.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/jquery-ui.js')}"></script>
<script type="text/javascript" src="${f:url('/js/jquery.jqGrid.min.js')}"></script>
<script type="text/javascript" src="${f:url('/js/grid.locale-ja.js')}"></script>
<script type="text/javascript" >
$(function(){
	$('#grid').jqGrid({
		 url:'/jqGridSample/customer/load'
	   	,datatype: "json"
        ,jsonReader:{repeatitems:false}
		,autoencode: true
		// 出力する項目の名称を設定する（ヘッダー）
	   	,colNames:['Id','Name','BirthDate']
		// 出力する項目の名称を設定する（明細行）
	   	,colModel:[
				 {name:'id',index:'id',width:200}
				,{name:'name',index:'name',width:250,editable:true,editoptions:{size:10}}
				,{name:'birthDate',index:'birthDate',width:250,editable:true,editoptions:{size:10}}
	   	 ]
		,height:'auto'
	   	,rowNum:10
	   	,rowList:[10,50,100]
	   	,pager:'#pager'
	   	,sortname:'id'
	    ,sortorder:'asc'
	    ,caption:"一覧"
	    ,viewrecords:true
	    ,editurl:"/jqGridSample/customer/entityDml"
	});
    $("#grid").jqGrid('navGrid','#pager'
		,{} // options
		,{reloadAfterSubmit:false} // edit options
		,{reloadAfterSubmit:true} // add options
		,{reloadAfterSubmit:true} // del options
		,{sopt:['eq','ne','cn','bw','ew','lt','gt']} // search options
	);
	$("#create").click(function(){
		$("#grid").jqGrid('editGridRow',"new",{reloadAfterSubmit:true});
	});
	$("#edit").click(function(){
		var gr = $("#grid").jqGrid('getGridParam','selrow');
		if　( gr != null ) $("#grid").jqGrid('editGridRow',gr,{reloadAfterSubmit:false});
		else alert("更新する行を選択して下さい");
	});
	$("#delete").click(function(){
		var gr = $("#grid").jqGrid('getGridParam','selrow');
		if　( gr != null ) $("#grid").jqGrid('delGridRow',gr,{reloadAfterSubmit:false});
		else alert("削除する行を選択して下さい");
	});
	$("#search").click(function(){
		$("#grid").jqGrid('searchGrid',{sopt:['eq','ne','cn','bw','ew','lt','gt']});
	});
})
</script>
</head>
<body>
<h1>Welcome to the Next Generation<br />〜JpGridSample～</h1>
<html:messages id="m" message="true">
<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span><strong>INFO:</strong>${f:h(m)}</p>
</div></div></html:messages>
<html:errors />

<input type="button" id="create" value="Create" />
<input type="button" id="edit" value="Edit Selected" />
<input type="button" id="delete" value="Delete Selected" />
<input type="button" id="search" value="Search" />
<table id="grid"></table>
<div id="pager"></div>

</body>
</html>