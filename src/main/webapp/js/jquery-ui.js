$(function(){
	// Accordion
	$( "#accordion" ).accordion({ header: "h3" });
	// radio
	$( "#radio" ).buttonset();
	// button
	$( "input:submit" ).button();
	$( "input:button" ).button();
	// checkbox
	$( "#check" ).button();
	$( "#format" ).buttonset();
	// Tabs
	$('#tabs').tabs();
	// Dialog
	$('#dialog').dialog({
		autoOpen: false,
		width: 600,
		buttons: {
			"Ok": function() {
				$(this).dialog("close");
			},
			"Cancel": function() {
				$(this).dialog("close");
			}
		}
	});
	// Dialog Link
	$('#dialog_link').click(function(){
		$('#dialog').dialog('open');
		return false;
	});
	// Datepicker
	$('#datepicker').datepicker({
		inline: true
	});
	//hover states on the static widgets
	$('#dialog_link, ul#icons li').hover(
		function() { $(this).addClass('ui-state-hover'); },
		function() { $(this).removeClass('ui-state-hover'); }
	);
});