package jp.co.seattleconsulting.service;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import org.seasar.extension.jdbc.OrderByItem;
import org.seasar.extension.jdbc.Where;
import org.seasar.framework.beans.util.BeanUtil;

import jp.co.seattleconsulting.dto.CustomerDto;
import jp.co.seattleconsulting.dto.JqGridDto;
import jp.co.seattleconsulting.entity.Customer;
import jp.co.seattleconsulting.form.AbstractForm;

import static jp.co.seattleconsulting.entity.CustomerNames.id;
import static org.seasar.extension.jdbc.operation.Operations.*;

/**
 * {@link Customer}のサービスクラスです。
 * 
 */
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.ServiceModelFactoryImpl" }, date = "2012/11/08 10:14:30")
public class CustomerService extends AbstractService<Customer> {

    /**
     * 識別子でエンティティを検索します。
     * 
     * @param id 識別子
     * @return エンティティ
     */
    public Customer findById(Integer id) {
        return select().id(id).getSingleResult();
    }

    /**
     * 識別子の昇順ですべてのエンティティを検索します。
     * 
     * @return エンティティのリスト
     */
    public List<Customer> findAllOrderById() {
        return select().orderBy(asc(id())).getResultList();
    }

    /**
     * JqGrid用データ取得処理
     */
    public JqGridDto<CustomerDto> fondAllForDto(AbstractForm customerForm) {

        /* --- where区 --- */
        Where where = and();
        if (customerForm._search) {
            where = getWhereClause(customerForm);
        }

        /* --- ソート設定 --- */
        OrderByItem obi = asc(id());
        if (SORD_ASC.equals(customerForm.sord)) {
            obi = asc(customerForm.sidx);
        } else if (SORD_DESC.equals(customerForm.sord)) {
            obi = desc(customerForm.sidx);
        }

        // データ取得位置
        int offset = Integer.parseInt(customerForm.page) * Integer.parseInt(customerForm.rows) - Integer.parseInt(customerForm.rows);
        // 件数取得
        long count = select().where(where).getCount();
        // 全ページ数
        String total = String.valueOf(count / Integer.parseInt(customerForm.rows));

        /* --- クエリー発行 --- */
        List<Customer> customerList = select().where(where).orderBy(obi).offset(offset).limit(Integer.parseInt(customerForm.rows)).getResultList();

        // 画面返却用dto
        List<CustomerDto> customerDtoList = new ArrayList<CustomerDto>();
        CustomerDto customerDto;
        for (Customer customer : customerList) {
            customerDto = new CustomerDto();
            BeanUtil.copyProperties(customer, customerDto);
            customerDto.birthDate = customerDto.birthDate.replace("-","/");
            customerDtoList.add(customerDto);
        }

        // jqGrid用Dtoで返却
        return new JqGridDto<CustomerDto>(total, customerForm.page, String.valueOf(count), customerDtoList);
    }
}