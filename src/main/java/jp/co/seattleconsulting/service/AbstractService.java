package jp.co.seattleconsulting.service;

import static org.seasar.extension.jdbc.operation.Operations.and;

import javax.annotation.Generated;

import jp.co.seattleconsulting.form.AbstractForm;

import org.seasar.extension.jdbc.Where;
import org.seasar.extension.jdbc.service.S2AbstractService;
import org.seasar.extension.jdbc.where.SimpleWhere;

/**
 * サービスの抽象クラスです。
 * 
 * @param <ENTITY> エンティティの型
 */
@Generated(value = { "S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.AbstServiceModelFactoryImpl" }, date = "2012/05/23 13:12:58")
public abstract class AbstractService<ENTITY> extends S2AbstractService<ENTITY> {
    /** 昇順ソート：ASC */
    public static String SORD_ASC = "asc";
    /** 降順ソート：DESC */
    public static String SORD_DESC = "desc";
    /** ～に等しい */
    public static String EQ = "eq";
    /** ～に等しくない */
    public static String NE = "ne";
    /** ～を含む */
    public static String CN = "cn";
    /** ～で始まる */
    public static String BW = "bw";
    /** ～で終わる */
    public static String EW = "ew";
    /** ～より小さい */
    public static String LT = "lt";
    /** ～より大きい */
    public static String GT = "gt";

    public Where getWhereClause(AbstractForm abstractForm) {
        Where where;
        if (EQ.equals(abstractForm.searchOper)) {
            // ～に等しい
            where = new SimpleWhere().eq(abstractForm.searchField, abstractForm.searchString);
        } else if (NE.equals(abstractForm.searchOper)) {
            // ～に等しくない
            where = new SimpleWhere().ne(abstractForm.searchField, abstractForm.searchString);
        } else if (CN.equals(abstractForm.searchOper)) {
            // ～を含む
            where = new SimpleWhere().like(abstractForm.searchField, "%" + abstractForm.searchString + "%");
        } else if (BW.equals(abstractForm.searchOper)) {
            // ～で始まる
            where = new SimpleWhere().starts(abstractForm.searchField, abstractForm.searchString);
        } else if (EW.equals(abstractForm.searchOper)) {
            // ～で終わる
            where = new SimpleWhere().ends(abstractForm.searchField, abstractForm.searchString);
        } else if (LT.equals(abstractForm.searchOper)) {
            // ～より小さい
            where = new SimpleWhere().le(abstractForm.searchField, abstractForm.searchString);
        } else if (GT.equals(abstractForm.searchOper)) {
            // ～より大きい
            where = new SimpleWhere().gt(abstractForm.searchField, abstractForm.searchString);
        } else {
            where = and();
        }
        return where;
    }
}