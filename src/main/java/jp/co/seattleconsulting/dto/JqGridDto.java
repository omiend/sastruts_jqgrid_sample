package jp.co.seattleconsulting.dto;

import java.util.List;

/**
 * JqGridで使用する為のDto
 * 
 * @param <E>
 */
public class JqGridDto<E> {

    public String total;
    public String page;
    public String records;
    public List<E> rows;

    public JqGridDto(String total, String page, String size, List<E> dtoList) {
        this.total = total;
        this.page = page;
        this.records = size;
        this.rows = dtoList;
    }
}
