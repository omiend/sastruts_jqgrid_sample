package jp.co.seattleconsulting.dto;

/**
 * CustomerDto
 */
public class CustomerDto extends AbstractDto {
    public String id;
    public String name;
    public String birthDate;
}
