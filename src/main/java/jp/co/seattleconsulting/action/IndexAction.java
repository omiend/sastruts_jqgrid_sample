package jp.co.seattleconsulting.action;

import org.seasar.struts.annotation.Execute;

/**
 * Index Action
 */
public class IndexAction extends AbstractAction {

	/**
	 * Index Method
	 */
	@Execute(validator = false)
	public String index() {
		return "index.jsp";
	}

	@Override
	public String load() {
		return null;
	}

	@Override
	public String entityDml() throws Exception {
		return null;
	}
}
