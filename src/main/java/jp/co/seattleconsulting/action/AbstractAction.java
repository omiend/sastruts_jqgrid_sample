package jp.co.seattleconsulting.action;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Abstract　Action
 */
public abstract class AbstractAction {

    @Resource
    public HttpServletRequest request;
    protected static final Logger logger = Logger.getLogger(AbstractAction.class.getName());

    public boolean isPost() {
        if (request.getMethod().equals("POST")) {
            logger.info("■ REQUEST METHOD IS POST");
            return true;
        } else if (request.getMethod().equals("GET")) {
            logger.info("■ REQUEST METHOD IS GET");
            return false;
        } else {
            logger.info("■ REQUEST METHOD IS OTHER");
            return false;
        }
    }
    
    public abstract String load();
    public abstract String entityDml() throws Exception;
}
