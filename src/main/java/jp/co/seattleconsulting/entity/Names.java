package jp.co.seattleconsulting.entity;

import javax.annotation.Generated;
import jp.co.seattleconsulting.entity.CustomerNames._CustomerNames;

/**
 * 名前クラスの集約です。
 * 
 */
@Generated(value = {"S2JDBC-Gen 2.4.45", "org.seasar.extension.jdbc.gen.internal.model.NamesAggregateModelFactoryImpl"}, date = "2013/05/08 14:39:22")
public class Names {

    /**
     * {@link Customer}の名前クラスを返します。
     * 
     * @return Customerの名前クラス
     */
    public static _CustomerNames customer() {
        return new _CustomerNames();
    }
}