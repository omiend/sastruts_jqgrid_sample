package jp.co.seattleconsulting.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Customer {

	@Id
    @GeneratedValue(strategy = GenerationType.TABLE)
	public Integer id;

	@Column
	public String name;

	@Column
    @Temporal(TemporalType.DATE)
	public Date birthDate;

}