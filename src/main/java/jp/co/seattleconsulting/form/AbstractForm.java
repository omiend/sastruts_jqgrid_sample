package jp.co.seattleconsulting.form;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.seasar.struts.util.ActionMessagesUtil;

public abstract class AbstractForm {

    @Resource
    public ActionMessages actionMessages = new ActionMessages();

    /** メッセージ有無フラグ */
    public boolean messageFlg = false;

    /**
     * メッセージをリクエストスコープにセットする
     * xxxForm.setRequestMessage(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("info.test"), request);
     */
    public void setRequestMessage(String messageType, ActionMessage actionMessage, HttpServletRequest request) {
        actionMessages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("info.test"));
        ActionMessagesUtil.addMessages(request, actionMessages);
        messageFlg = true;
    }

    /**
     * メッセージをセッションスコープにセットする
     */
    public void setSessionMessage(String messageType, ActionMessage actionMessage, HttpSession Session) {
        actionMessages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("info.test"));
        ActionMessagesUtil.addMessages(Session, actionMessages);
        messageFlg = true;
    }

    /* --- jqGrid用パラメータ --- */
    /** 【jqGrid用パラメータ 】呼出メソッド識別：add/edit/del */
    @Resource
    public String oper;
    /** 【jqGrid用パラメータ 】ソート対象項目 */
    @Resource
    public String sidx;
    /** 【jqGrid用パラメータ 】ソート方法：asc/desc */
    @Resource
    public String sord;
    /** 【jqGrid用パラメータ 】検索フラグ：true/false */
    @Resource
    public Boolean _search;
    /** 【jqGrid用パラメータ 】検索対象フィールド */
    @Resource
    public String searchField;
    /** 【jqGrid用パラメータ 】検索方法： */
    @Resource
    public String searchOper;
    /** 【jqGrid用パラメータ 】検索値 */
    @Resource
    public String searchString;
    /** 【jqGrid用パラメータ 】ページングインデックス */
    @Resource
    public String page;
    /** 【jqGrid用パラメータ 】リミットサイズ */
    @Resource
    public String rows;

    protected static final Logger logger = Logger.getLogger(AbstractForm.class.getName());
}
