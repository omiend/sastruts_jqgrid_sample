package util;

public class Constants {
    /** add(新規登録):jqGridの機能で渡ってくる機能名称 */
    public static final String OPER_ADD = "add";
    /** edit(更新):jqGridの機能で渡ってくる機能名称 */
    public static final String OPER_EDIT = "edit";
    /** del(削除):jqGridの機能で渡ってくる機能名称 */
    public static final String OPER_DEL = "del";
}
