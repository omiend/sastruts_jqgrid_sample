create table ID_GENERATOR (
    PK varchar(255) not null,
    VALUE bigint not null,
    constraint ID_GENERATOR_PK primary key(PK)
);
