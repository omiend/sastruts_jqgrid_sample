create table CUSTOMER (
    ID integer not null,
    NAME varchar(255),
    BIRTH_DATE date,
    constraint CUSTOMER_PK primary key(ID)
);
